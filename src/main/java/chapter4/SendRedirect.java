package chapter4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SendRedirect extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String text = req.getParameter("text");
		if (text != null && text.trim().length() > 0) {
			PrintWriter writer = resp.getWriter();
			writer.write(text);
			if (Boolean.parseBoolean(req.getParameter("flush"))) {
				writer.flush();
			}
			if (Boolean.parseBoolean(req.getParameter("close"))) {
				writer.close();
			}
			if (Boolean.parseBoolean(req.getParameter("redirect"))) {
				resp.sendRedirect(text);
			}
			if (Boolean.parseBoolean(req.getParameter("dispatcherReq"))) {
				RequestDispatcher dispatcher = req.getRequestDispatcher(text);
				dispatcher.forward(req, resp);
			}
			if (Boolean.parseBoolean(req.getParameter("dispatcherCtx"))) {
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(text);
				dispatcher.forward(req, resp);
			}
		} else {
			resp.getWriter().println("You need to send a valid text");
		}
	}
}
