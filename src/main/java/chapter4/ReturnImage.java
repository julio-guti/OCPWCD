package chapter4;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

public class ReturnImage extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setContentType("image/png");

		ServletContext ctx = getServletContext();
		InputStream inputStream = ctx.getResourceAsStream("/images/megacat.png");

		int read;
		byte[] bytes = new byte[1024];

		ServletOutputStream outputStream = resp.getOutputStream();
		while ((read = inputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}

		outputStream.flush();
		outputStream.close();
	}
}
