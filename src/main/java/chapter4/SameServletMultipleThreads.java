package chapter4;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SameServletMultipleThreads extends HttpServlet {

    private final SimpleDateFormat _spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String initTime = getCurrentTime();
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("[servlet: ");
        sb.append(this);
        sb.append("]\n[thread: ");
        sb.append(Thread.currentThread());
        sb.append("]\n[request: ");
        sb.append(req);
        sb.append("]\n[response: ");
        sb.append(resp);
        sb.append("]\nTimes: ");
        sb.append(initTime);
        sb.append(getCurrentTime());

        resp.getWriter().print(sb.toString());
    }

    private String getCurrentTime() {
        return " (" + _spf.format(new Date()) + ") ";
    }
}