package chapter4;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class RequestParameters extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().println(processRequest(req));
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().println(processRequest(req));
	}

	private String processRequest(HttpServletRequest req) {
		StringBuilder sb = new StringBuilder();
		sb.append("req.getHeader(\"User-Agent\"); = ");
		sb.append(req.getHeader("User-Agent"));
		sb.append('\n');
		sb.append("req.getCookies() = ");
		sb.append(Arrays.toString(req.getCookies()));
		sb.append('\n');
		sb.append("req.getSession(); = ");
		sb.append(req.getSession().toString());
		sb.append('\n');
		sb.append("req.getMethod(); = ");
		sb.append(req.getMethod());
		sb.append('\n');
		sb.append("req.getParameter(\"color\"); = ");
		sb.append(req.getParameter("color"));
		sb.append('\n');
		sb.append("req.getParameterValues(\"color\"); = ");
		sb.append(Arrays.toString(req.getParameterValues("color")));
		sb.append('\n');
		sb.append("req.getParameter(\"sizes\"); = ");
		sb.append(req.getParameter("sizes"));
		sb.append('\n');
		sb.append("req.getParameterValues(\"sizes\"); = ");
		sb.append(Arrays.toString(req.getParameterValues("sizes")));
		sb.append('\n');
		return sb.toString();
	}
}