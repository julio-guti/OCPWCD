package chapter4;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServletLife extends HttpServlet {

	private final SimpleDateFormat _spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private StringBuilder _sb = new StringBuilder("Step 1. Load class" + getCurrentTime() + "\n");

	public ServletLife() {
		//So basically this is wrong, if you need initialize something, use the init method instead.
		_sb.append("Step 2. Instantiate servlet (constructor rungs)").append(getCurrentTime()).append(
			"\n\tYour servlet class-no-args constructor runs (you should NOT write a constructor; just use the compiler-supplier default).\n");
	}

	@Override
	public void init() throws ServletException {
		_sb.append("Step 3. init()").append(getCurrentTime())
			.append("\n\tCalled only ONCE in the servlet's life, and must complete before Container can call service().\n");
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//Is not very common overwrite this method
		_sb.append("Step 4. service()").append(getCurrentTime()).append(
			"\n\tThis is where the servlet spends most of its life (this one takes of call doGet(), doPost(), etc) (Each request runs in a separate thread).\n");
		super.service(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().print(_sb.toString());
		_sb = new StringBuilder();
	}

	@Override
	public void destroy() {
		//This Servlet would never print this out, it's too late.
		//I have never seen this....
		System.err.println("Step 5. destroy()" + getCurrentTime());
	}

	private String getCurrentTime() {
		return " (" + _spf.format(new Date()) + ") ";
	}
}