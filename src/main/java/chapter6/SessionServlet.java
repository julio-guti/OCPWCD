package chapter6;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class SessionServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();

		boolean createNewSession = Boolean.parseBoolean(req.getParameter("createNewSession"));
		HttpSession session = req.getSession(createNewSession);
		out.println("The actual session: " + session);

		if (session != null) {
			out.println("new session :" + session.isNew());

			out.println("creation time: " + session.getCreationTime());
			out.println("last accessed time: " + session.getLastAccessedTime());

			String maxInactiveInterval = req.getParameter("maxInactiveInterval");
			if (maxInactiveInterval != null && maxInactiveInterval.trim().length() > 0) {
				session.setMaxInactiveInterval(Integer.parseInt(maxInactiveInterval));
			}
			out.println("max inactive interval: " + session.getMaxInactiveInterval());

			if (Boolean.parseBoolean(req.getParameter("invalidateSession"))) {
				session.invalidate();
				out.println("The session just die, if a call is perform after this you will run into an illegalStateException");
			}
		}
	}
}