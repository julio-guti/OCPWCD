package chapter6;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CookieServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String cookieName = req.getParameter("cookieName");
		if (cookieName != null && cookieName.trim().length() > 0) {
			Cookie newCookie = new Cookie(cookieName, req.getParameter("cookieValue"));
			newCookie.setMaxAge(Integer.parseInt(req.getParameter("cookieMaxAge")));
			resp.addCookie(newCookie);
		}

		PrintWriter out = resp.getWriter();
		for (Cookie cookie : req.getCookies()) {
			out.println("Cookie [name: " + cookie.getName() + "] [value : " + cookie.getValue() + "] [maxAge:" + cookie.getMaxAge() + "]");
		}
		out.flush();
		out.close();
	}
}
