package chapter10;

import javax.servlet.jsp.tagext.TagSupport;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Classic1 extends TagSupport {

    private final SimpleDateFormat _spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private StringBuilder _sb = new StringBuilder("Step 1. Load class" + getCurrentTime() + "<br>");

    private String getCurrentTime() {
        return " (" + _spf.format(new Date()) + ") ";
    }
}
