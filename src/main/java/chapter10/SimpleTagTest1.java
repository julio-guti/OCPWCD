package chapter10;

import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleTagTest1 extends SimpleTagSupport {

    private final SimpleDateFormat _spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private StringBuilder _sb = new StringBuilder("Step 1. Load class" + getCurrentTime() + "<br>");

    public SimpleTagTest1() {
        super();
        _sb.append("Step 2. Instantiate class (no-args constructor runs)");
        _sb.append(getCurrentTime());
        _sb.append("<br>");
    }

    @Override
    public void setJspContext(JspContext jspContext) {
        _sb.append("Step 3. Call the setJspContext(JspContext) method");
        _sb.append(getCurrentTime());
        _sb.append("<br>");
        super.setJspContext(jspContext);
    }

    @Override
    public void setParent(JspTag jspTag) {
        _sb.append("Step 4. Call setParent(JspTag) method");
        _sb.append(getCurrentTime());
        _sb.append("<br>");
        super.setParent(jspTag);
    }

    @Override
    public void setJspBody(JspFragment jspFragment) {
        _sb.append("Step 6. Call setJspBody(JspFragment) method");
        _sb.append(getCurrentTime());
        _sb.append("<br>");
        super.setJspBody(jspFragment);
    }

    @Override
    public void doTag() throws JspException, IOException {
        _sb.append("Step 7. doTag() ");
        _sb.append(getCurrentTime());
        _sb.append("<br>");
        getJspContext().getOut().print(_sb.toString());
        _sb = new StringBuilder();
    }

    private String getCurrentTime() {
        return " (" + _spf.format(new Date()) + ") ";
    }
}
