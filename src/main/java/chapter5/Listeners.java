package chapter5;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class Listeners implements ServletContextAttributeListener, HttpSessionListener, ServletRequestListener, ServletRequestAttributeListener,
	HttpSessionBindingListener, HttpSessionAttributeListener, ServletContextListener, HttpSessionActivationListener {

	/////////////// ServletContextAttributeListener
	@Override
	public void attributeAdded(ServletContextAttributeEvent scab) {
		System.out.println("ServletContextAttributeListener.attributeAdded [event: " + scab + "] [value: " + scab.getValue() + "]");
	}

	@Override
	public void attributeRemoved(ServletContextAttributeEvent scab) {
		System.out.println("ServletContextAttributeListener.attributeRemoved [event: " + scab + "] [value: " + scab.getValue() + "]");
	}

	@Override
	public void attributeReplaced(ServletContextAttributeEvent scab) {
		System.out.println("ServletContextAttributeListener.attributeReplaced [event: " + scab + "] [value: " + scab.getValue() + "]");
	}
	/////////////// ServletContextAttributeListener

	/////////////// HttpSessionListener
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("HttpSessionListener.sessionCreated [event:" + se + "]");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		System.out.println("HttpSessionListener.sessionDestroyed [event:" + se + "]");
	}
	/////////////// HttpSessionListener

	/////////////// ServletRequestListener
	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("ServletRequestListener.requestDestroyed [event:" + sre + "]");
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("ServletRequestListener.requestInitialized [event:" + sre + "]");
	}
	/////////////// ServletRequestListener

	/////////////// ServletRequestAttributeListener
	@Override
	public void attributeAdded(ServletRequestAttributeEvent srae) {
		System.out.println("ServletRequestAttributeListener.attributeAdded [event: " + srae + "] [value: " + srae.getValue() + "]");
	}

	@Override
	public void attributeRemoved(ServletRequestAttributeEvent srae) {
		System.out.println("ServletRequestAttributeListener.attributeRemoved [event: " + srae + "] [value: " + srae.getValue() + "]");
	}

	@Override
	public void attributeReplaced(ServletRequestAttributeEvent srae) {
		System.out.println("ServletRequestAttributeListener.attributeReplaced [event: " + srae + "] [value: " + srae.getValue() + "]");
	}
	/////////////// ServletRequestAttributeListener

	/////////////// HttpSessionBindingListener
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		System.out.println("HttpSessionBindingListener.valueBound [event: " + event + "]");
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		System.out.println("HttpSessionBindingListener.valueUnbound [event: " + event + "]");
	}
	/////////////// HttpSessionBindingListener

	/////////////// HttpSessionAttributeListener
	@Override
	public void attributeAdded(HttpSessionBindingEvent se) {
		System.out.println("HttpSessionAttributeListener.attributeAdded [event: " + se + "] [value: " + se.getValue() + "]");
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent se) {
		System.out.println("HttpSessionAttributeListener.attributeRemoved [event: " + se + "] [value: " + se.getValue() + "]");
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent se) {
		System.out.println("HttpSessionAttributeListener.attributeReplaced [event: " + se + "] [value: " + se.getValue() + "]");
	}
	/////////////// HttpSessionAttributeListener

	/////////////// ServletContextListener
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("ServletContextListener.contextInitialized [event:" + sce + "]");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("ServletContextListener.contextDestroyed [event:" + sce + "]");
	}
	/////////////// ServletContextListener

	/////////////// HttpSessionActivationListener
	@Override
	public void sessionWillPassivate(HttpSessionEvent se) {
		System.out.println("HttpSessionActivationListener.sessionWillPassivate [event:" + se + "]");
	}

	@Override
	public void sessionDidActivate(HttpSessionEvent se) {
		System.out.println("HttpSessionActivationListener.sessionDidActivate [event:" + se + "]");
	}
	/////////////// HttpSessionActivationListener
}
