package chapter5;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GetInitParameters extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StringBuilder sb = new StringBuilder();

		sb.append("To the given DD :\n");
		sb.append("    <context-param>\n");
		sb.append("        <param-name>adminEmail</param-name>\n");
		sb.append("        <param-value>awesome@test.com</param-value>\n");
		sb.append("    </context-param>\n");
		sb.append("    <servlet>\n");
		sb.append("        <servlet-name>GetInitParameters</servlet-name>\n");
		sb.append("        <servlet-class>chapter5.GetInitParameters</servlet-class>\n");
		sb.append("        <init-param>\n");
		sb.append("            <param-name>adminEmail</param-name>\n");
		sb.append("            <param-value>notSoAwesome@test.com</param-value>\n");
		sb.append("        </init-param>");
		sb.append("    </servlet>\n");
		sb.append("    <servlet-mapping>\n");
		sb.append("        <servlet-name>GetInitParameters</servlet-name>\n");
		sb.append("        <url-pattern>/chapter5/GetInitParameters</url-pattern>\n");
		sb.append("    </servlet-mapping>");
		sb.append('\n');
		sb.append('\n');
		sb.append(" getServletContext().getInitParameter(\"adminEmail\") =  ");
		sb.append(getServletContext().getInitParameter("adminEmail"));
		sb.append('\n');
		sb.append(" getServletConfig().getInitParameter(\"adminEmail\") =  ");
		sb.append(getServletConfig().getInitParameter("adminEmail"));
		sb.append('\n');

		resp.getWriter().println(sb.toString());
	}
}
