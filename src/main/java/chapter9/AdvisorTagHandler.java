package chapter9;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class AdvisorTagHandler extends SimpleTagSupport {

	private String user;

	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.write("Hi " + user + "<br> " + getAdvice());
	}

	public void setUser(String user) {
		this.user = user;
	}

	private String getAdvice() {
		String[] adviceString = {"advice 1", "advice 2", "yeah yeah"};
		int random = (int) (Math.random() * adviceString.length);
		return adviceString[random];
	}
}
