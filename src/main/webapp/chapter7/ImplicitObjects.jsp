<%@page isErrorPage="true" %>

<%!
    public void jspInit() {
        System.out.println("Override jspInit, CALLED ONCE");
    }

    public void jspDestroy() {
        System.err.println("Override jspDestroy");
    }
%>

<html>
    <body>
        <h1>Implicit Objects</h1>
        <label>out</label>
        <br>
        <%= out %>
        <br>
        <br>
        <label>request</label>
        <br>
        <%= request %>
        <br>
        <br>
        <label>response</label>
        <br>
        <%= response %>
        <br>
        <br>
        <label>session</label>
        <br>
        <%= session %>
        <br>
        <br>
        <label>application</label>
        <br>
        <%= application %>
        <br>
        <br>
        <label>config</label>
        <br>
        <%= config %>
        <br>
        <br>
        <label>exception (only for error pages)</label>
        <br>
        <%= exception %>
        <br>
        <br>
        <label>pageContext</label>
        <br>
        <%= pageContext %>
        <br>
        <br>
        <label>page</label>
        <br>
        <%= page %>
    </body>
</html>