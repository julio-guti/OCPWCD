<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title></title>
    </head>
    <body>

        <jsp:useBean id="person" class="chapter8.Person">
            <jsp:setProperty name="person" property="*" />
        </jsp:useBean>
        <jsp:getProperty name="person" property="name" />

    </body>
</html>