<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <body>
        <h1>Core Jstl</h1>
        <label>out without spaceXml</label>
        <br>
        <c:out value="<b> Tip </b>" />
        <br>
        <label>out whit spaceXml = true</label>
        <br>
        <c:out value="<b> Tip </b>" escapeXml="true" />
        <br>
        <label>out whit spaceXml = false</label>
        <br>
        <c:out value="<b> Tip </b>" escapeXml="false" />
        <br>
    </body>
</html>